package com.azabani;
import gui.twod.ChemDoodleLauncher;

public class ChemPoodle {
	private static synchronized void log(String format, Object... values) {
		Thread thread = Thread.currentThread();
		StackTraceElement caller = thread.getStackTrace()[2];
		System.err.printf(caller.getFileName() + ":" +
		                  caller.getLineNumber() + ": " +
		                  format, values);
	}
	public static void main(String[] arguments) {
		log("old java.vm.name: %s\n", System.getProperty("java.vm.name"));
		System.setProperty("java.vm.name", "Java HotSpot(TM) 64-Bit Server VM");
		log("new java.vm.name: %s\n", System.getProperty("java.vm.name"));
		gui.twod.ChemDoodleLauncher.main(arguments);
	}
}
