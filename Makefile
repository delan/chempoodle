.POSIX:

WHERE := ../ChemDoodle
IN    := ChemPoodle.class

install: $(WHERE)/ChemDoodle.jar. $(WHERE)/ChemDoodle. ChemPoodle.class
	cd $(WHERE) && cp ChemDoodle. ChemDoodle..
	jar -uef com.azabani.ChemPoodle $(WHERE)/ChemDoodle.jar com/azabani/ChemPoodle.class
	cd $(WHERE) && egrep -v '^(JAVA_HOME|PATH)=' ChemDoodle. > ChemDoodle..
	cd $(WHERE) && sed 's/^\$$JAVA_HOME\/bin\///' ChemDoodle.. > ChemDoodle

$(WHERE)/ChemDoodle.jar.:
	cd $(WHERE) && cp ChemDoodle.jar ChemDoodle.jar.

$(WHERE)/ChemDoodle.:
	cd $(WHERE) && cp ChemDoodle ChemDoodle.

ChemPoodle.class:
	javac -d . -cp $(WHERE)/ChemDoodle.jar ChemPoodle.java

clean:
	rm -rf com $(WHERE)/ChemDoodle..
	cd $(WHERE) && cp ChemDoodle. ChemDoodle
	cd $(WHERE) && cp ChemDoodle.jar. ChemDoodle.jar

.PHONY: install clean
